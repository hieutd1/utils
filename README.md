# README #


## bitbucket-maven-publish.gradle ##

Sử dụng để kiểm tra maven repo đã tồn tại trên bitbucket chưa và tạo maven file cho library. File sau khi tạo phải up thủ công lên repo.

### Project level directory ###
* Edit **gradle.properties**, add follow codes

```
#!properties

COMPANY=your_bitbucket_username_or_team
REPOSITORY_NAME=your_repo
REVERSION_CODE=newest_reversion or branch_name
```


* Create file **bitbucket_login.properties** contain bitbucket's *username* and *password* and add it to **.ignore** for safe.

```
#!properties

USERNAME=your_username
PASSWORD=your_password
```

* Edit **.ignore**, add follow lines of code


```
#!git

/bitbucket_login.properties
```
## Module level directory ##

* build.gradle 

add this:

```
#!gradle
...
apply from: 'https://bitbucket.org/hieutd1/utils/raw/master/bitbucket-maven-publish.gradle'
...
```

[Above link](https://bitbucket.org/hieutd1/utils/raw/master/bitbucket-maven-publish.gradle) is gradle scribuild  files

* gradle.properties

add:


```
#!properties

VERSION_NAME=your_artifact_version

ARTIFACT_NAME=your_artifact_name
ARTIFACT_PACKAGE=your_artifact_package
ARTIFACT_PACKAGING=aar (also use jar)
```

*build.gradle

edit:

```
#!gradle

...
defaultConfig {
        ...
        versionName '1.0.0'
    }
...
```

to


```
#!gradle

...
defaultConfig {
        ...
        versionName VERSION_NAME
    }
...
```

for the same version_name of android_library and maven

## After all ##

Open **cmd** or **terminal** on your project dir path then run:


```
#!bash

gradlew uploadArchives
```

After finish, push all on <project_dir>/maven to git.

Now, you can use

```
#!html

https://api.bitbucket.org/1.0/repositories/{bitbucket_username}/{repo_slug}/raw/{reversion_or_branch}/maven/
```

as maven repo.